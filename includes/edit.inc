<?php
/**
 * @file
 * Provides filtering of encoded URLs in fields being edited based on settings.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Implements hook_field_attach_load().
 *
 * @todo
 * This hook is called on entity view and edit. In both cases, each filter
 * process callback has already been invoked. Because the filtered text is in
 * the safe_value key which is rendered on entity view, we do not need to do
 * anything further on the view case.
 *
 * On entity edit, the unfiltered text (from the value key) is displayed in the
 * textarea field. In order for someone to edit this text or for an image to
 * display in a WYSIWYG editor, the portable path filters need to be processed
 * on the raw text value.
 *
 * How can we determine if this hook is being invoked as part of an edit?
 *
 * On an user entity, the safe_value omits the style attribute on image tags?
 *
 * @deprecated
 */
function _portable_path_field_attach_load($entity_type, $queried_entities, $age, $options) {
return;
  // Get information.
  $info = entity_get_info($entity_type);
  $formats = filter_formats();
  $settings = portable_path_config();
  $filter_info = portable_path_filter_info();

  // An entity may not have a bundle key, e.g., user.
  $bundle_key = $info['entity keys']['bundle'];
  foreach ($queried_entities as &$entity) {
    $bundle_name = isset($entity->$bundle_key) ? $entity->$bundle_key : NULL;
    $instances = field_info_instances($entity_type, $bundle_name);
    if (isset($instances[$entity_type])) {
      // Array structure is one level deeper on user than node.
      // User has $instances['user']['field_description'].
      $instances = $instances[$entity_type];
    }
    foreach ($instances as $instance) {
      if (in_array($instance['field_name'], $settings['field_names'])) {
        $field_name = $instance['field_name'];
        // Load the field to read its columns and use first column for value.
        $field = field_info_field($instance['field_name']);
        $column = current(array_keys($field['columns']));
        // Get the language key from the field array.
        $keys = array_keys($entity->$field_name);
        if (empty($keys)) {
          // Field is not set, i.e., no value.
          continue;
        }
        $language = current($keys);
        $format = $entity->{$field_name}[$language][0]['format'];
        $filters = filter_list_format($format);
        // Which of our filters is active in the format and in what order?
        $our_filters = array_filter($filters, 'portable_path_array_filter');

        if (empty($our_filters)) {
          continue;
        }

        if (!empty($entity->$field_name) && !empty($entity->{$field_name}[$language][0][$column])) {
          foreach (array_keys($our_filters) as $key) {
            $function = $filter_info[$key]['process callback'];
            if (function_exists($function)) {
              $filter = $our_filters[$key];
              $entity->{$field_name}[$language][0][$column] = $function($entity->{$field_name}[$language][0][$column], $filter, $formats[$format]);
            }
          }
        }
      }
    }
  }
}

/**
 * Implements hook_field_widget_form_alter().
 *
 * This hook is called on entity edit. By default, the unfiltered text (e.g.,
 * from the value or summary column) is displayed in the textarea field. In
 * order for a user to edit this text or for an image to display in an WYSIWYG
 * editor, the portable path filters need to be applied to the raw text value.
 *
 * Unlike the media module which does its business using JavaScript (applying
 * its filter logic to render the WYSIWYG output, but displaying the raw token
 * element in the source view), our filters can simply be applied to the raw
 * input. The file source will be converted to a portable token on entity save.
 *
 * If this module is used with remote_content, this approach allows filters to
 * be applied consistently on the local site during entity view instead of on
 * the remote site during entity load. To enable this clean separation of the
 * content bits from the render bits, one of the text filters will need to have
 * its 'cache' property set to zero to disable the caching of the filtered text.
 * This separation is important with filters like media_wysiwyg and picture that
 * go beyond simple text filtering and apply rendering changes.
 */
function _portable_path_field_widget_form_alter(&$element, &$form_state, $context) {
  $delta = $context['delta'];
  if (empty($context['items']) || empty($context['items'][$delta])) {
    // Field is not set, i.e., no value.
    return;
  }

  $settings = portable_path_config();
  if (!in_array($context['instance']['field_name'], $settings['field_names'])) {
    // Filters are not to be applied to this field.
    return;
  }

  // Extract information.
  $items = $context['items'];
  if (empty($items[$delta]['format'])) {
    // Text processing is set to 'plain' on this instance; nothing to do.
    return;
  }

  $format = $items[$delta]['format'];
  // The first column does not have an extra level in the element array.
  $column = current($element['#columns']);

  // Gather information.
  $formats = filter_formats();
  $filter_info = portable_path_filter_info();

  $filters = filter_list_format($format);
  // Which of our filters is active in the format and in what order?
  $our_filters = array_filter($filters, 'portable_path_array_filter');

  if (empty($our_filters)) {
    return;
  }

  // @todo At this point we could call:
  //   $function = $instance['widget']['module'] . '_field_widget_form';
  // to regenerate the form element and let it deal with array structures and
  // naming conventions.
  // OR, we can assume all fields are like text fields with or without summary
  // and just set the #default_value.

  // Apply filters to non-format columns.
  $extra_columns = array_diff($element['#columns'], array('format'));
  foreach ($extra_columns as $extra_column) {
    foreach (array_keys($our_filters) as $key) {
      $function = $filter_info[$key]['process callback'];
      if (function_exists($function)) {
        $filter = $our_filters[$key];
        if ($extra_column == $column) {
          $default_value = &$element['#default_value'];
        }
        else {
          $default_value = &$element[$extra_column]['#default_value'];
        }
        if ($default_value) {
          $default_value = $function($default_value, $filter, $formats[$format]);
        }
      }
    }
  }
}
